# -*- coding: UTF-8 -*-
from sikuli import *
import random

myScriptPath = ".\\"
addImportPath(myScriptPath)
import images,util

from someClasses import *
from util import ptlog,try_hl


## 比率變數 宣告一律丟到 someClasses 存放，改完 someClasses.py ，記得重開IDE



#============================================
# @ exec : 在後勤瘋狂亂按 達成 後勤成功的可能
# @ param : R 遊戲畫面
# @ return : None 是否會回傳?
def retardaDellegation(R):
    # 這裡的名稱 要跟 def 後面一樣
    funcName = "myFunction"
    util.ptlog("[step] : " + funcName + " __Start...")

    ch =   ConstantHolder()
    # 用來亂點的區塊
    retardaRegion = util.getRelativeSubRegion(R,ch.for_retardaDellegation_ratio)
    #請務必在後勤介面
    
    try_click_region = util.getRegionByImages(R,images.dellgation_LT)
    # 可以亂點的地方歸零的地方 就是 ak87
    ak87 = Region( try_click_region.getX()+25, try_click_region.getY()+100, int(try_click_region.getW()/2),  try_click_region.getH()*4 )
    
    while True:
        # 亂點歸零
        for i in range(2):
            util.randClickRegion(ak87)
            
        # 隨便點 主要區域
        util.randClickRegion(retardaRegion)
        # 看到取消
        if  R.exists(images.dellgation_cancel,2):
            # 回到頭部
            continue
        # 如果看到 推薦，就去按他
        if R.exists(images.dellgation_susume,2):
            util.randClick(R,images.dellgation_susume, 2)
            # 如果無法滿足
            if R.exists(images.cannot_enough_Logistics,2):
                # 回到頭
                continue
            # 如果看到開始
            if R.exists(images.dellgation_start,2):
                util.randClick(R,images.dellgation_start, 2)
                wait(1)
                continue
        if not exists(images.dellgation_LT,3):
            wait(180)
    util.ptlog("[step] : " + funcName + " __End...")
#============================================
# @ exec : 從主畫面到  委託列表
# @ param : R 遊戲畫面
# @ return : None 
# 說明: 模板 function
def goInDellegation(R):
    funcName = "goInDellegation"
    util.ptlog("[step] : " + funcName + " __Start...")
    
    ptlog("[step][info] : 嘗試按下 左上方箭頭...")
    # 左上方箭頭的 Region
    lt_R = util.getRegionByImages(R,images.open_logistics)
    util.click_it_until_appear(R,lt_R,images.go_forward_icon,3)
    ptlog("[step][info] : 嘗試按下 左上方箭頭  成功")
    
    ch = ConstantHolder()
    # 獲取 軍事委託的 Region
    nnn = ch.delegation_region_of_mainWindow_ratio
    delegation_region = util.getRelativeSubRegion(R,nnn)
    # 得到"前往"icon的 Region
    icon_region = util.getRegionByImages(delegation_region,images.go_forward_icon)
    
    ptlog("[step][info] : 嘗試按下 軍事委託的前往...")
    util.click_it_until_appear(R,icon_region,images.dellgation_LT,3)
    ptlog("[step][info] : 嘗試按下 左上方箭頭  成功!!")   

    ptlog("[step][info] : 在委託的下方 多案幾個下...")   
    try_click_region = util.getRegionByImages(R,images.dellgation_LT)
    after_offset = Region(
            try_click_region.getX()+25,
            try_click_region.getY()+100,
            int(try_click_region.getW()/2), 
            try_click_region.getH()*4 
            )
    after_offset.highlight("#FFFFFF")
    for i in range(3):
        util.randClickRegion(after_offset)
    after_offset.highlightAllOff()
    util.ptlog("[step] : " + funcName + " __End...")
#============================================
# @ exec : 檢查是否在後勤介面，是的話回傳 後勤的 + 號 有幾個。
# @ param : R 遊戲區域
# @ return :  (int >= 0 ) or (Failed =  -1)!!
def getNumbers_OF_PlusInLogisticsInterface(R):
    # 這裡的名稱 要跟 def 後面一樣
    funcName = "getNumbers_OF_PlusInLogisticsInterface"
    util.ptlog("[step] : " + funcName + " __Start...")

    # 回傳結果
    result = None
    
    if R.exists(images.dellgation_LT,5):
        ptlog("[step][info] : 嘗試尋找加號...")
        wait(1)
        if R.exists(images.plus_icon_InLogisticsInterface,3):
            ptlog("[step][found] : 找到加號")
            # 放 match list 的
            tmp = []
            # =======
            setFindFailedResponse(SKIP)
            mh = list(R.findAll(Pattern(images.plus_icon_InLogisticsInterface).similar(0.70)))
            # =======
            result = len(mh)
        else:
           ptlog("[step][warn] : 尚未找到加號") 
           result = 0
    #================ 耍白癡摟~~~
    else:
        ptlog("[step][warn] : 幹你娘老雞掰 你根本不再後勤介面 看雞雞!!")
        result = -1
    # = End
    util.ptlog("[step] : " + funcName + " __End...")
    return result
#============================================

# @ exec : 撤退，從海戰地圖上撤退
# @ param : R 遊戲畫面
# @ param : currentMap  一開始選關卡 的 img
#  EX: 截有 "2-4 救援約克鎮"  的圖標[images.stage_2_4]
# @ return : None 
def withDrawFromMap(R,currentMap):
    # 這裡的名稱 要跟 def 後面一樣
    funcName = "withDrawFromMap"
    ptlog("[step] : " + funcName + " __Start...")
    # 按下撤退
    util.click_it_until_appear(R,images.withdraw,images.appear_Commission_msg_chk,3)
    # 檢查是否退出了
    util.click_it_until_appear(R,images.appear_Commission_msg_chk,currentMap,3)
    
    ptlog("[step] : " + funcName + " __End...")
#============================================
# @ exec : 按下敵方 的小弟船，每次呼叫都是點不同的船船~~
# @ param : R 遊戲畫面
# @ return : [None] or [那位小弟的Region] or [Boss Region](特殊情形時)
def  fightXiaoLouLuo(R,magicRatio=None,Magic_X_Y=[0,0]):
    funcName = "fightXiaoLouLuo"
    util.ptlog("[step] : " + funcName + " __Start...")
    
    #回傳
    ret = None
    # 旗標
    afterMoveFlag = False
    
    # 永久迴圈
    while True:
        # 把小弟列出來
        suckShip_list = findAnemyShips(R,magicRatio,Magic_X_Y)
        # 如果存在 小弟
        if suckShip_list != -1:
            # 幾位小弟? 共target_total 位
            target_total = len(suckShip_list)
            # 隨便找 1 位小弟 的代碼
            suckShip_idx = random.randint(0,target_total-1)
            # 找到小老弟的 Region 瞜 
            iWantFuckAnemy = suckShip_list[suckShip_idx]
            # 去幹 那位小弟
            util.randClickRegion(iWantFuckAnemy)
            # 出現無法到達目標點
            clickCnt = 0
            while  R.exists(images.cannot_arrive_anemy,2):
                suckShip_idx += 1
                iWantFuckAnemy = suckShip_list[(suckShip_idx+1)%target_total]
                ptlog("[step][warn] : 改下一個 idx = " +  str((suckShip_idx+1)%target_total))
                util.randClickRegion(iWantFuckAnemy)
                ptlog("[step][info] : 已經嘗試點擊")
                if not  R.exists(images.cannot_arrive_anemy,3):
                    return iWantFuckAnemy
                clickCnt += 1
                if clickCnt > 10 :
                    # 嘗試 移動視角
                    ptlog("[step][info] : 已經 多次點擊到達一定次數，嘗試移動視角...")
                    wait(2)
                    moveView(R,magicRatio,Magic_X_Y)
                    afterMoveFlag = True
                    break
                    # 離開此 while [exists]
            # end while    
            if  afterMoveFlag != True:
                # 準備回傳 
                ret = iWantFuckAnemy
                # 離開 while [True]
                break
            else:
                afterMoveFlag = False
                continue
                # 重新 開始此函式，因為已經調整 視角
        else:
            # 此時 suckShip_list = -1
            util.ptlog("[step][warn] : 不存在可以點的小弟...")
            # 如果有找到BOSS，嘗試回傳 BOSS Region
            if R.exists(images.boss_icon,3):
                ret = util.getRegionByImages(R,images.boss_icon)
            else:    
                ret = suckShip_list
            # 離開 while [True]
            break
    # End While [True]
    # ============================================
    util.ptlog("[step][info] :  記得呼叫 chkCanInBattle ~") 
    util.ptlog("[step] : " + funcName + " __End...")
    return ret
#============================================
# 呼叫前提，已經有點中 敵方目標，並且我方角色 在"移動中"!!
# @ exec : 執行完，必定可以在 準備戰鬥之畫面...
# 遇到敵方襲擊 預設 迴避!!
# @ param : R 遊戲畫面
# @param wantMoveRegion: 你現在正要前往的目的地
def chkCanInBattle(R,wantMoveRegion,magicRatio=None,Magic_X_Y=[0,0]):
    funcName = "chkCanInBattle"
    util.ptlog("[step] : " + funcName + " __Start...")
    sp_cnt = 0
    while True:
        arrivalCode = chkArrival(R)
        if arrivalCode == 1:
            ptlog("[info]: 準備戰鬥!!")
            # 確定 有 出擊 圖標
            break
        #==============================    
        # 有敵方襲擊!!
        elif arrivalCode == 2:
            ptlog("[warn]: 有敵方襲擊!!")
            # 嘗試規避
            util.click_it_until_vanish(R,images.avoid,3)
            # 檢查是否 規避失敗
            if R.exists(images.weigh_anchor_long,3):
                ptlog("[step][warn]: 規避失敗!!")
                # 但是因為還是進入了 戰鬥準備畫面 所以可以離開
                break
            else:
                ptlog("[step][info]: 規避成功!!") 
                util.randClickRegion(wantMoveRegion)
                continue
        #====================================    
        # 又被飛機打了
        elif arrivalCode == 0:
            wait(5)        
            ptlog("[warn]: 還待在地圖，重新選擇  目標")
            if sp_cnt <= 1 :
                util.randClickRegion(wantMoveRegion)
            else:
                fightXiaoLouLuo(R,magicRatio,Magic_X_Y)
            sp_cnt+=1
            continue
        #====================================    
        # 過不去RRRR
        elif arrivalCode == 3:
            ptlog("[warn]: 還待在地圖，因為過不去")
            #去打 小弟(suckShip)
            fightXiaoLouLuo(R,magicRatio,Magic_X_Y)
            continue
        #====================================    
        # 未知的 ret 代碼
        else:
            ptlog("[ERROR]:  八咖那!! 不可能到這裡的， 不~~~可~~~能~~~~!!!")
            exit(-1)
    # End while
    util.ptlog("[step] : " + funcName + " __End...")
#============================================

# @ exec : 回傳在走道目標的途中  [空襲]:0，[隨機遇敵]:2，[被擋住]:3
#    可能發生的 代碼~~
# @ param : R  遊戲畫面
# @ return : 1(成功)/ 2有敵人來找你 / 0 :仍然在地圖畫面，可能遇到空襲
# 這邊新增代碼， chkCanInBattle 也要跟著改喔
def chkArrival(R):
    # 這裡的名稱 要跟 def 後面一樣
    funcName = "chkArrival"
    util.ptlog("[step] : " + funcName + " __Start...")
    ret = 0
    timeCnt = 0
    while timeCnt < 12:
        # 如果有看到 規避!!
        if R.exists(images.avoid,0):
            ret =  2
            break
        else:
            ptlog("[chkArrival][status] : 沒看到規避")
        #=============== 下面移位~~~    
        # 如果看到 出擊~~ 
        if R.exists(images.weigh_anchor_long,0): 
            ret = 1
            break
        else:
            ptlog("[chkArrival][status] : 沒看到出擊")
        #=============== 下面移位~~~    
        # 如果看到 到不了
        if R.exists(images.cannot_arrive_anemy,0): 
            ret = 3
            break
        else:
            ptlog("[chkArrival][status] : 沒看到 無法到達")
        #=============== 沒有下面移位了
        #計數器 +1
        timeCnt += 1 
        ptlog("[chkArrival][status] : 已經確認 = " + str(timeCnt) + " 次")
        wait(0.3)
    # end while 
    msg = ""
    if ret ==  1:
        msg = "成功進入戰鬥畫面!"
    elif ret == 2 :
        msg = "遇到敵方襲擊"
    elif ret == 0 :
        msg = "尚未進入戰鬥，可能遇到空襲"
    elif ret == 3:
        msg = "你被擋住了!!"
    else:
        msg = "[ERROR] : 八咖哪!!! ret 代碼錯誤!!"
    util.ptlog("[step] chkArrival  = " + str(msg))
    util.ptlog("[step] : " + funcName + " __End...")
    return ret
#============================================
# @ exec : 確定進去 某張地圖...，如果你已經進去你就是87
# @ param : R 遊戲畫面
# @ param : stage_Name 你自己爽就好
# @ param : stage_image 該關卡圖案
# @ return : 正常進入 回傳0/ 你是 87時，回傳87
def goIn_X_X(R,stage_Name,stage_image):
    # 這裡的名稱 要跟 def 後面一樣
    funcName = "goIn_X_X"
    util.ptlog("[step] : " + funcName + " __Start...")
    # 如果 發現有 撤退
    if R.exists(images.withdraw,1):
        ptlog("[step][info] : 已經在地圖內~")  
        util.ptlog("[step] : " + funcName + " __End...")
        return 87
    # =================確定沒有耍白癡 檢查完畢=======================   
    ptlog("[step][info] : 檢查是否有  " + str(stage_Name))
    if R.exists(stage_image,10):
        ptlog("[step][info] : 存在 !!")
    else:
        ptlog("[step][WARN] : 不存在 !!")
        ptlog("[step][info] : 請協助引導至 "+ str(stage_Name) +" 關卡選擇話面...")
        while not R.exists(stage_image,1):
            wait(1)
    #============== 
    ptlog("[step][info] : 準備點選  " + str(stage_Name))
    util.click_it_until_appear(R,stage_image,images.immediate_start,3)

    ptlog("[step][info] : 準備點選 立即前往...")        
    util.click_it_until_appear(R,images.immediate_start,images.water_ship,3)    
    
    #  這邊 如果人物臉黑 他會告知，所以也要做 判斷
    while not R.exists(images.withdraw,3):
        ptlog("[step][info] : 準備 再次點選 立即前往...")    
        util.randClick(R,images.immediate_start, 3)
            
        # 如果有出現確定按鈕...(提示人物臉黑的): PS 當然也有可能 是別的確定，但是不管~~~
        ptlog("[step][info] : 檢查是否有訊息提示需要按下  確定...")    
        if R.exists(images.half_exp_msg,3):
            ptlog("[step][warn] : 嘗試按下確定...")    
            util.randClick(R,images.appear_Commission_msg_chk, 3)
        else:
            ptlog("[step][info] : 尚未看到訊息...")    
    #util.click_it_until_appear(R,images.immediate_start,images.withdraw,3)
    #======================================================================
    
    ptlog("[step][info] : 已經在地圖內~")  
    util.ptlog("[step] : " + funcName + " __End...")
    return 1
#============================================
# @ exec : 處理 戰鬥與結算 ，他會處理戰鬥中的 問題??，
#     以及打完以後出現的訊息、確定、獎勵、鎖定....等
# @ param : R 遊戲畫面
# @ param : All_IN 預設關閉，如開啟代表，不管船臉黑不黑，都會繼續打下去。
# @ return : 0(沒有進入到戰鬥畫面，需要重新選)/ 1(成功打完一戰)
def deal_battle_and_result(R,All_IN = False):
    funcName = "deal_battle_and_result"
    util.ptlog("[step] : " + funcName + " __Start...")
    
    # 檢查是否在準備出擊
    ptlog("[step]確認是否在 出擊畫面...")
    if R.exists(images.weigh_anchor_long,10):
        ptlog("[step][info] 確認~ 即將出擊!!")
    else:
        ptlog("[step][warn] 可能有異常 偵測不到  出擊按鈕!! ")
        return 0
    # 按下 出擊
    ptlog("[step][info] : 準備按下出擊! ") 
    util.click_it_until_vanish(R,images.weigh_anchor_long,3)
    ptlog("[step][info] : 已經進入戰鬥 ")
    #============ <測試 不確定是否加入在這邊>
    if All_IN and R.exists(images.half_exp_msg,3):
        ptlog("[step][warn] : ALL IN 已經啟動")
        util.randClick(R,images.appear_Commission_msg_chk,3)
    else:
        ptlog("[step][info] : ALL IN 尚未啟動")
    #============ </測試 不確定是否加入在這邊>

    # 是某要確定 自動打架有開??
    # TODO

    # 等待打完的圖標出現
    ptlog("[step][info] : 等待 戰鬥完畢")    
    wait(15)
    while not R.exists(images.touch_to_continue,3):
        wait(3)
    # 等待打完的圖標出現
    if R.exists(images.touch_to_continue,10):    
        ptlog("[step][info] : 確定打完了!")    
        
    
    # 戰鬥評價
    ptlog("[step][info] : 準備按下 點擊繼續! ") 
    #util.click_it_until_vanish(R,images.touch_to_continue,1)
    util.click_it_until_appear(R,images.touch_to_continue,images.get_items_touch_to_continue,3)
    ptlog("[step][info]: 已經按下 點擊繼續")            
    
    # 獲得道具
    if R.exists(images.get_items_touch_to_continue,3):
        ptlog("[step][info]: 有獲得道具")

        ptlog("[step][info]: 按下 點擊繼續 mini")        
        util.click_it_until_vanish(R,images.get_items_touch_to_continue,1)
        # 有獲得到 新的船
        if R.exists(images.share_when_get_new_ship,3):
            ptlog("[step][YA]:  ★★★★★") 
            ptlog("[step][YA]:  獲得船!!")
            ptlog("[step][YA]:  ★★★★★") 
            
            # 生成 一個中間區域 來當隨機點的地方
            ch = ConstantHolder()
            center_region = util.getRelativeSubRegion(R,ch.appear_Commission_ratio)
            while R.exists(images.share_when_get_new_ship,3):
                util.randClickRegion(center_region)
                wait(2)
            #需要鎖定
            while R.exists(images.chk_to_lock_ship_msg,3):
                util.randClick(R,images.appear_Commission_msg_chk,3)
                wait(2)
            ptlog("[step]:  確定跳過 獲得新船")
        # 沒有新船
        else:
            ptlog("[step][info]:  沒有新船")        
    else:
        ptlog("[step][info]: \"沒有\" 獲得道具")
    # End 獲得 [道具/新船]
    
    # 進入結算
    # 按下 確定
    ptlog("[step]: 進入如結算畫面，按下確定")
    if R.exists(images.yellow_chk,10):
        pass
    util.click_it_until_vanish(R,images.yellow_chk,3)
    ptlog("[step][info]: 多等 3 秒，檢查有無多餘的確定...")
    wait(3)
    # if R.exists(,3):
    util.ptlog("[step] : " + funcName + " __End...")
    return 1
#============================================
# @ exec: 嘗試 點 BOSS的圖案，如果存在的話。
# @ param: R = 遊戲視窗(Region) 
# @ return:  無法點選 BOSS 圖標 回傳  0；
# 可以點 也點下去了回傳 1，必定會在準備戰鬥畫面；
# 點了      但過不去    回傳 2 。
def try_toClickBoss(R,magicRatio=None,Magic_X_Y=[0,0]):
    # 特別檢查
    if R.exists(images.weigh_anchor_long,2):
        return 1
    
    funcName = "try_toClickBoss"
    util.ptlog("[step] : " + funcName + " __Start...")
    # 寫下你要的動作...
    # 等等要回傳的值
    ret = 0
     #wantMoveRegion = util.getRegionByImages(R,images.boss_icon)
    if R.exists(images.boss_icon, 1):
        R.getLastMatch().highlight(1)
        boss_region = util.getRegionByImages(R,images.boss_icon) 
        util.randClickRegion(boss_region)
        if R.exists(images.cannot_arrive_anemy,2):
            ret = 2
        else:
            ret = 1
            # 因為點了Boss
            chkCanInBattle(R,boss_region,magicRatio,Magic_X_Y)
        #==============
        ptlog("[step][info]: 點了 BOSS...")
        chkArrival(R)
    else:
        ptlog("[step][warn]: 找不到 BOSS...")

    util.ptlog("[step] : " + funcName + " __End...")
    return ret
#============================================
# @ exec : 回傳 地圖上的所有敵方存活船艦 的 Region
# @ param: R = 遊戲視窗(Region)
# @ return: Region-List or 如果都不存在 回傳 -1
def findAnemyShips(R,magicRatio=None,Magic_X_Y=[0,0]):
    # 這裡的名稱 要跟 def 後面一樣
    funcName = "findAnemyShip"
    util.ptlog("[step] : " + funcName + " __Start...")
    # 常數物件 ch 
    ch = ConstantHolder()

    # <做 R 的改變 >
    tmp_ratio = ch.true_of_battle_region_ratio
    sub_R = util.getRelativeSubRegion(R,tmp_ratio)
    ptlog("[step][info] : highlight sub!")
    sub_R.highlight(1)
    # </做 R 的改變 >
    
    sub_R.hover(sub_R.getTopLeft())
    wait(0.5)

    # total anamy count
    anamyCount = 0
    # 這兩個變數需要按照順序喔~
    anamyScaleName  = ch.anamyScaleName
    anamyImages = ch.anamyImages

    if len(anamyScaleName) != len(anamyImages):
        ptlog("[step][ERROR]:  請檢查敵人種類的數量 ，List 數量 不一致!!!")
        exit(-1)
    
    # 等等要回傳 的 RegionList
    resultRegionList = []
    
    # 需要被 "多方搜尋" 的 敵人，如果去 findAll 不存在的 圖片 會出現錯誤。
    #  "多方搜尋" : 表示 螢幕上可能有多個一樣的 圖片。
    # 變數名稱為， 確實存在的敵人 的idx (可以用來對應，anamyImages & anamyScaleName)
    exactlyExistsAnamyIdx = []
    
    # 尋找各種規模的 敵軍 (小、中、大)
    for idx, targetScale in enumerate(anamyImages):
        ptlog("[step][info] :  開始搜尋 " + str(anamyScaleName[idx])  + "   敵人..." )
        # 先預設沒有敵人
        scale_found = 0
        # 如超過 指定次數 就不搜尋， 去找下一個 規模的敵人
        scanTimes = ch.anemy_scan_times
        notFoundCnt = 0
        ptlog("[step][info] :  如超過 \"" + str(scanTimes)  + "\" 次搜尋次數 就跳下一個規模之敵人!!" )
        # 還沒超過 scanTimes 都要繼續找
        while notFoundCnt  < scanTimes:
            ptlog("[info][step] 使用 相似度 " + str(ch.anemy_icon_similarList[idx]) + " 來搜尋  " + str(anamyScaleName[idx]) )
            wait(0.5)
            if sub_R.exists(Pattern(targetScale).similar(ch.anemy_icon_similarList[idx]),1):
                # 將上方宣告的 變數 設定為 1 ；表示有找到
                scale_found = 1
                break
            ptlog("[info][step] 第 " + str(notFoundCnt+1) + " 次搜尋 " +  str(anamyScaleName[idx])  )
            notFoundCnt +=1
        # End while
        if scale_found == 0 :
            ptlog("[step][warn] : 尚未找到 " +  str(anamyScaleName[idx])  )
        else:
            ptlog("[step]: 存在  " +   str(anamyScaleName[idx])  + " 之敵人!!")
            # 增加等等要搜尋的敵人，加入圖片
            exactlyExistsAnamyIdx[len(exactlyExistsAnamyIdx):] = [ idx ]
    #  End For
    #  ================  已經知道誰需要被搜尋，接下來 多方尋找
    
    
    # 小中大 的的敵人分別用的顏色~~
    colorList = ch.colorList
    
    # 敵人有幾個 這邊顏色就要有幾個~~
    if len(colorList) !=  len(anamyImages):
        ptlog("[step][ERROR]:  請增加 色碼數量 !!!")
        exit(-1)
        
        
    for idx in exactlyExistsAnamyIdx:
        ptlog("matchScore = " + str(ch.anemy_icon_similarList[idx]))
        try :
            for anamy in sub_R.findAll(Pattern(anamyImages[idx]).similar(ch.anemy_icon_similarList[idx])):
                
                ptlog("標記 " + str(anamyScaleName[idx]) + " 之敵人...")
                anamyCount += 1
                #anamy.highlight(colorList[idx])

                #  X Y ，就是 黃色的點 座標
                ptlog( "X : " +  str(anamy.getTarget().getX()) )
                ptlog( "Y : " + str(anamy.getTarget().getY()) )
                
                # 我們透過 加上 固定 偏移量，想辦法 獲得 新的 白框 Region
                # 請看例子，Ctrl + T 按下方圖片
                # "HowToKnowsOffset.png"
                
                # 此數值 指的是 ΔX,ΔY
                offset_X = ch.offsetAnamyIconToShip[0]
                offset_Y = ch.offsetAnamyIconToShip[1]
                # 白框的 長跟寬
                anamy_W = ch.anamy_W_H[0]
                anamy_H = ch.anamy_W_H[1]
                # 算出實際上白框的位置
                tmp_region = Region(
                        anamy.getTarget().getX()  + offset_X - int(anamy_W/2)-5, \
                        anamy.getTarget().getY() + offset_Y - int(anamy_H/2),\ 
                        anamy_W,\
                        anamy_H)
                # 將結果 存放至  list
                resultRegionList[len(resultRegionList):] = [tmp_region]
            #for End
        except:
            ptlog("[WARN] exception  PASS:沒有"+ anamyScaleName[idx] +"的敵軍")
            ptlog("[WARN] 照理來說 不得走到這邊，因為已經用 Exists檢查過了，不可能不存在")            
    #======================================
    # 三種規模 已經搜尋完畢
    for yes in resultRegionList:
        yes.highlight(ch.color_of_anamy_frame)
        
    util.ptlog("[util][info]: \'所有\' 規模 已標記... 總共 " + str(anamyCount) + " 個標記~")
    util.ptlog("[step] : " + funcName + " __End...")
    if anamyCount == 0:
        # 嘗試移動畫面
        ptlog("[step][info] 嘗試移動畫面...")
        moveView(R,magicRatio,Magic_X_Y)
        wait(2)
        util.ptlog("[util][warn]: RETURN -1")
        return -1
    else:
        util.ptlog("[util][info]: RETURN RegionList")
        wait(1.5)
        highlightAllOff()
        return resultRegionList
#============================================
# 只有在主畫面 時才能 呼叫
# @ return 遊戲畫面的 Region
def getMainRegion():
    funcName = "getMainRegion"
    util.ptlog(funcName + " __Start...")

    # 回傳 結果 
    ret = None
    
    param = ConstantHolder()
    
    if ( exists(images.weigh_anchor,3) ): 
        util.ptlog("[found] step: 已經在主畫面找到 黃色出擊 圖標...123")

        # 使用圖片
        tmp_r = util.getRegionByImages(Screen(0),images.weigh_anchor)
        x = tmp_r.getX()
        y = tmp_r.getY()
        w = tmp_r.getW()
        h = tmp_r.getH()
        # util.ptlog(" param.list_autoRelocatedParam : " + str(param.list_autoRelocatedParam))
        nx = int(x - param.list_autoRelocatedParam[0])
        ny = int(y - param.list_autoRelocatedParam[1])
        nw = int(w* param.list_autoRelocatedParam[2])
        nh = int(h* param.list_autoRelocatedParam[3])
        
        ret = Region(nx,ny,nw,nh)
    else:
        util.ptlog("[ERROR] step: 無法在主畫面找到 黃色出擊 圖標...")
        ret = None
    # end if
    util.ptlog(funcName + " __End...")    
    return ret        
#============================================
# @ exec : 回傳 一個 Region，
# @ param: R 遊戲區域
# @ return : Region?
def getRegion_appear_Commission_msg(R):
    # 這裡的名稱 要跟 def 後面一樣
    funcName = "getRegion_appear_Commission_msg"
    util.ptlog("[step] : " + funcName + " __Start...")
    ch = ConstantHolder()
    ch.appear_Commission_ratio

    ret = util.getRelativeSubRegion(R,ch.appear_Commission_ratio)
    
    util.ptlog("[step] : " + funcName + " __End...")
    return ret
#============================================
# @ exec : 去打某一關卡，以打王為優先，如果中途發生問題，撤退後重新進入
# @ param :R 遊戲畫面
# @ param : wantMap  關卡 的 img
#  EX: 截有 "2-4 救援約克鎮"  的圖標[images.stage_2_4]
# @ param : mapChinese 關卡的中文 隨便你爽
# @ param :magicRatio 如果有指定 會在一開始的時候幫你調整 畫面區域
# (magicRatio 得出的 Region ，左下拖曳至右上)
# @ param : Magic_X_Y : [X,Y] : X,Y 都為整數，可以不給(右上點的額外偏移量)。
# @ param :  NeedSupply : True、(預設是 False)；如果有給True，他會在彈藥為2時候 去撿~
# @ param :  crazyMod : True = 沒子彈去輾爆BOSS，而且不管 人爽不爽， False = 俗辣模式沒子彈就撤退
# @ param :  All_IN : 預設關閉=False，如開啟=True，代表，不管船臉黑不黑，都會繼續打下去。
# @ return :  None 
def loop_X_X_stage(R, wantMap,mapChinese, magicRatio = None,Magic_X_Y = [0,0],NeedSupply = False,crazyMod = False,All_IN = False):
    # 這裡的名稱 要跟 def 後面一樣
    funcName = "loop_X_X_stage"
    util.ptlog("[step] : " + funcName + " __Start...")
    # 寫下你要的動作...
    goIn_X_X(R,mapChinese,wantMap)
    wait(5)

    # 開始調整視角...
    moveView(R,magicRatio,Magic_X_Y)
    if False: # 棄用此 區塊
        if magicRatio is not None:
            # 利用這 magicR，左下 + 右上點 做 拖曳
            magicR = util.getRelativeSubRegion(R,magicRatio)
            # 調慢移動速度
            Settings.MoveMouseDelay = 2
            
            # 決定是否要更多偏移
            new_RT  = Location (magicR.getTopRight().getX()+Magic_X_Y[0],magicR.getTopRight().getY()+Magic_X_Y[1])
            # 本來是R.dragDrop(magicR.getBottomLeft(),magicR.getTopRight())
            R.dragDrop(magicR.getBottomLeft(),new_RT)
            # 條快
            Settings.MoveMouseDelay = 0.5
        else:
            ptlog("[step][info]  :  無須調整視角")
     # 調整視角完畢
    if crazyMod == True:
        oilQuota = 999
        ptlog("[step][warn]  :  你已經開啟瘋狂模式")
    else: 
        oilQuota = 5
        ptlog("[step][info] : 你已開啟普通模式")
    # 如果還出不來 X-Y，表示沒有看到 X-Y 的作戰地圖名稱。也就代表著還在 海上地圖
    while not exists(wantMap,3):
        # 嘗試去幹 BOSS
        fuckBoss = try_toClickBoss(R,magicRatio,Magic_X_Y)
        if  fuckBoss == 0 or fuckBoss == 2:
            # 去打小弟，不是小弟被打完了，就是 你被卡住了
            if oilQuota == 1:
                ptlog("[warn]: 你的油，不夠打BOSS...")
                # 呼叫撤隊
                # 然後continue...[其實是要呼叫 break，跳掉內部的while]
                # 從 wantMap 撤退
                withDrawFromMap(R,wantMap)
                break
                #exit(1)
            # ===================
            # 你還有油去打小弟
            else:
                oilQuota -=1

                #點一艘 小弟船
                iWantFuckAnemy = fightXiaoLouLuo(R,magicRatio,Magic_X_Y)
                #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 檢查 是否 真的有小弟可以點
                if  iWantFuckAnemy != -1:
                    #只要有點移動船隻的動作都要呼叫這個
                    chkCanInBattle(R,iWantFuckAnemy,magicRatio,Magic_X_Y)
                    deal_battle_and_result(R,All_IN)
                    # 走到這邊絕對 會打完一戰
                    # <檢查是否要補油  開始>
                    if NeedSupply == False :
                        ptlog("[step][info] : 此關卡 使用者 不要求 補給...")
                        pass
                    else:
                        #============================== 檢查 是否 要補給
                        if  oilQuota  > 2 :
                            ptlog("[step][info] : 子彈充足 不須補給...")
                            # 返回 while
                            continue
                        else:
                            ptlog("[step][warn] : 子彈低於 3 須補給...")
                            # 往下走
                            pass
                        #==============================
                        # ************************************************
                        # !!!  走到這裡時 BOSS 可能會出現，所以不能太早找這個固定REGION !!!
                        # 在 大型地圖中 BOSS 出現時 螢幕會整個 偏移，所以要先等待
                        ptlog("[step][info] : 等待 5 秒...")
                        wait(5)
                        
                        ptlog("[step][info] : 嘗試尋找子彈圖標...")
                        if R.exists(images.supplyBullet,10):
                            ptlog("[step][found]找到子彈圖標...")    
                            bulletIconRegion = util.getRegionByImages(R,images.supplyBullet)
                            # 要做偏移
                            this_ch = ConstantHolder()
                            #得到偏移量
                            dtY = this_ch.bulletOffset
                            # 真正可以點的地方
                            trueBulletRegion = Region(
                                    bulletIconRegion.getX(),\
                                    bulletIconRegion.getY()+dtY,\
                                    bulletIconRegion.getW(),\
                                    bulletIconRegion.getH())
                            util.randClickRegion(trueBulletRegion)
                            # 因為這是一種移動 所以必須呼叫 
                            chkCanInBattle(R,trueBulletRegion,magicRatio,Magic_X_Y)
                            # 這其實會有 BUG 但就算了ㄅ，ㄏ!!
                            oilQuota += 3
                        else:
                            ptlog("[step][warn]尚未找到子彈圖標... 請確認 此關卡是否有 子彈圖標...")     
                            wait(3)
                        # ************************************************
                        # </檢查是否要補油 完畢>
                    continue
                else:
                    ptlog("[ERROR]: 沒小弟船可以點...")                            
                    print("[ERROR]: 沒小弟船可以點...")
                    # exit(-1)
                    # 從 目前地圖中撤退，但先檢查
                    ptlog("[step][info] : 嘗試檢查 是否有規避 5秒...")
                    if R.exists(images.avoid,5):
                        ptlog("[step][info] : 按下規避...")
                        util.randClick(R,images.avoid,3)
                        continue
                    elif R.exists(images.boss_icon,2):
                        ptlog("[step][info] :  仍然發現 BOSS")
                        continue
                    else:
                        ptlog("[warn]: 撤退")
                        withDrawFromMap(R,wantMap)
                        break
                #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        # end IF
        else:
            # 打完就沒有了~ 走到這就是去打BOSS了
            deal_battle_and_result(R,All_IN)
            break
        #End else

    util.ptlog("[step] : " + funcName + " __End...")
#============================================
# 在地圖上時才會呼叫
# @ exec : 移動畫面
# @ param : R: 遊戲畫面
# @ param : magicRatio 移動畫面的Region比率，會從此Region 的左下滑動到右上
# @ param : Magic_X_Y 如果有給定，他會在magicRatio 生成的右上點(Location)再加上此參數
# @ return : None 
def moveView(R,magicRatio=None,Magic_X_Y=[0,0]):
    funcName = "myFunction"
    util.ptlog("[step] : " + funcName + " __Start...")
    if magicRatio is not None:
        # 利用這 magicR，左下 + 右上點 做 拖曳
        magicR = util.getRelativeSubRegion(R,magicRatio)
        # 調慢移動速度
        Settings.MoveMouseDelay = 2
        
        # 決定是否要更多偏移
        new_RT  = Location (magicR.getTopRight().getX()+Magic_X_Y[0],magicR.getTopRight().getY()+Magic_X_Y[1])
        # 本來是R.dragDrop(magicR.getBottomLeft(),magicR.getTopRight())
        R.dragDrop(magicR.getBottomLeft(),new_RT)
        # 條快
        Settings.MoveMouseDelay = 0.5
    else:
        ptlog("[step][info]  :  無須調整視角")
    # 調整視角完畢
    util.ptlog("[step] : " + funcName + " __End...")
#============================================
## 模板 如果有需要 請按照 這裡的格式作改變
# @ exec : 寫下這函式執行目的
# @ param : 他可能需要的參數
# ...
# @ return : None 是否會回傳?
# 說明: 模板 function
def myFunction():
    # 這裡的名稱 要跟 def 後面一樣
    funcName = "myFunction"
    util.ptlog("[step] : " + funcName + " __Start...")
    # 寫下你要的動作...
    
    util.ptlog("[step] : " + funcName + " __End...")
#============================================
    
############# 以下 給 憲憲 用###################

# @ exec : 接收後勤委託獎勵
# @param :R遊戲視窗
# @ return : None 
#在主畫面的時候才能呼叫
def harvestLogistics(R):
    # 這裡的名稱 要跟 def 後面一樣
    funcName = "harvestLogistics"
    util.ptlog("[step] : " + funcName + " __Start...")
 
    ch = ConstantHolder()
    
    ptlog("[step][found]: 開始檢查大艦隊")
    if R.exists(images.big_fleet_button,5):
        ptlog("[step][info]: 找到了大艦隊，你在主畫面")
        ptlog("[step][found]: 檢查+圖案...")
        logistics_icon_pattern = Pattern(images.logistics_icon).similar(0.78)
        if R.exists(logistics_icon_pattern,5):
            util.getRegionByPattern(R,logistics_icon_pattern)
            logistics_icon_region = util.getRegionByPattern(R,logistics_icon_pattern)
            logistics_icon_region.highlight(1)
            ptlog("[step][info]: 找到了+圖案")
            ptlog("[step][info]: 打開後勤選單")
            util.click_it_until_appear(R,images.open_logistics,images.logistics_completion_button,2)
            ptlog("[step][info]: 打開後勤選單成功")
            ptlog("[step][info]: 嘗試點擊橘色完成的按鈕")
            util.click_it_until_appear(R,images.logistics_completion_button,images.Successful_commission,2)
            ptlog("[step][info]: 嘗試點擊橘色完成的按鈕成功")
            ptlog("[step][found]: 檢查有沒有多個後勤完成")
            while R.exists(images.Successful_commission,5):
                ptlog("[step][info]: 你有後勤成功歸來!!!!!")
                ptlog("[step][info]: 嘗試點擊委託成功的畫面")
                util.click_it_until_appear(R,images.Successful_commission,images.get_items_touch_to_continue,5)
                ptlog("[step][info]: 嘗試點擊委託成功的畫面成功")
                ptlog("[step][info]: 嘗試點擊獲得道具按鈕")
                util.randClick(R,images.get_items_touch_to_continue,3)
                ptlog("[step][info]: 嘗試點擊獲得道具按鈕成功")
                wait(3)
            else:
                ptlog("[step][warn]: 你沒有後勤回來~~~~")
                ptlog("[step][info]: 嘗試回到主選單")
                while not R.exists(images.big_fleet_button,3):
                    ptlog("[step][warn]: 你還沒回到主選單")
                    main_window = util.getRelativeSubRegion(R,ch.main_window)
                    util.randClickRegion(main_window)
                    ptlog("[step][info]:點了主畫面一下 ")
                else:
                    ptlog("[step][info]: 你在主選單")
                
                
        else:
            ptlog("[step][warn]: 沒有+號，你沒有可完成的後勤")
    else:
        ptlog("[step][warn]: 沒有大艦隊，你不在主畫面")

    util.ptlog("[step] : " + funcName + " __End...")

# @ exec : 全自動且聰明的後勤
# @ param : 他可能需要的參數
def smartDellegation(R):
    funcName = "smartDellegation"
    util.ptlog("[step] : " + funcName + " __Start...")
    ch = ConstantHolder()    
    #海關代碼
    Through = smartDellegationInspector(R)  
    
    #聰明的後勤海關
    smartDellegationInspector(R)
    
    if Through == 1:
        pass
    elif Through ==-1:
        ptlog("[step][info]:點擊下方")
        smartDellegation_below_Region = util.getRelativeSubRegion(R,ch.smartDellegation_below_ratio)
        util.randClickRegion(smartDellegation_below_Region)
        wait(3)
        util.randClickRegion(smartDellegation_below_Region)
    
    util.ptlog("[step] : " + funcName + " __End...")

# @ exec : 聰明的後勤海關
# @ param :R
def smartDellegationInspector(R):
    funcName = "smartDellegationInspector"
    util.ptlog("[step] : " + funcName + " __Start...")
    # 寫下你要的動作...
    
    ch = ConstantHolder()
    #還未執行委託的圖案
    unexecuted_Logistics = images.unexecuted_Logistics
    unexecuted_Logistics_Pattern = Pattern(unexecuted_Logistics).similar(0.85)
    while True:
        ptlog("[step][info]:尋找可以執行的後勤... ")
        if not R.exists(unexecuted_Logistics_Pattern,3):
            ptlog("[step][info]:沒有看到可以執行的後勤回傳-1")
            pass_Through = -1
            return pass_Through
        else:
            ptlog("[step][info]:看到可以執行的後勤回傳1")
            pass_Through = 1
            return pass_Through
     #未完成的坑
    
    util.ptlog("[step] : " + funcName + " __End...")
    
# @ exec : 找到腳色頭上那一格
# @param :R遊戲視窗
# @ return : None 
#有腳色的彈藥在畫面內才能使用
def findThePlayerHeadUp(R):
    funcName = "findThePlayerHeadUp"
    util.ptlog("[step] : " + funcName + " __Start...")

    ptlog("[step][info]:尋找腳色(開始找彈藥圖示)")
    if R.exists(Pattern(images.remaining_Ammunition).similar(0.80),3):
        remainingAmmunitionRegion = util.getRegionByImages(R,images.remaining_Ammunition)
        ptlog("[step][info]:開始偏移")
        res_region = Region(remainingAmmunitionRegion.getX()+75,remainingAmmunitionRegion.getY(),remainingAmmunitionRegion.getW(),remainingAmmunitionRegion.getH())
        ptlog("[step]:show region")
        res_region.highlight(1)

    else:
        ptlog("[step][Failed]:畫面上沒有腳色(畫面上找不到腳色的彈藥!)")
        pass

    util.ptlog("[step] : " + funcName + " __End...")    
#============================================



