# -*- coding: UTF-8 -*-
from sikuli import *
myScriptPath = ".\\"
addImportPath(myScriptPath)

import util,images

# 有修改這邊變數記得充新開啟 IDE...
class ConstantHolder():
    # 建構 各種內參數
    def __init__(self):
        # 自動擷取 遊戲視窗用的 參數  [dtx, dty ,w, h]　# 0.186657 [1146, 329, 8.67261, 4.8809]
        self.list_autoRelocatedParam = [1146,329,8.67261,4.8809]
        # 敵方 規模名稱 下面這兩個要按照順序!!
        self.anamyScaleName  = ["小型規模","中型規模", "大型規模"]
        # 規模名稱 對應的圖片
        self.anamyImages = [images.mini_scale, images.middle_scale, images.big_scale]
        # 敵方規模 的 相似度 參數[小,中,大]
        self.anemy_icon_similarList = [0.85,0.80,0.75]
        # 嘗試搜尋次數，如超過 N 次就 換下一個 規模的敵人
        self.anemy_scan_times = 3
        # 用來標記不同 規模敵人的 顏色 LIST
        self.colorList = ["#0ffc00","#1442ff","#f91900"]
        # 用來d標記敵方船艦的 框框顏色
        self.color_of_anamy_frame = "#0ffc00"
        # 從 敵方 規模的 "小圖" 偏移到 該船 中心的 偏移量
        self.offsetAnamyIconToShip = [60,65]
        # 從 敵方 船 的 W(寬)，跟H(高)
        self.anamy_W_H = [95,67]
        # 出現緊急委託 訊息 可能的區域 比率常數
        self.appear_Commission_ratio =  [ 0.276596, 0.321951, 0.446122, 0.320732 ]
        # 地圖之中 2-4 約克鎮 的 Region 的比率常數
        self.stege_2_4_ratio = [ 0.2814, 0.771951, 0.142759, 0.0707317 ]
        #  調整 2-4 視角 用的特殊參數
        self.stege_2_4_view_ratio = [ 0.387097, 0.57561, 0.0960879, 0.139024 ]
        #  調整 1-4 視角 用的特殊參數
        self.stege_1_4_view_ratio = [ 0.356211, 0.589024, 0.126287, 0.0390244 ]        
        #  調整 3-4 視角 用的特殊參數
        self.stege_3_4_view_ratio = [ 0.387097, 0.57561, 0.0960879, 0.139024 ]
        #  調整 6-4 視角 用的特殊參數
        self.stege_6_4_view_ratio = [ 0.201098, 0.387805, 0.096774, 0.002439 ]
        #  調整 3-2 視角 用的特殊參數
        self.stege_3_2_view_ratio = [ 0.201098, 0.387805, 0.096774, 0.002439 ]
        #  調整 5-1 視角 用的特殊參數
        self.stege_5_1_view_ratio = [ 0.201098, 0.387805, 0.096774, 0.002439 ]
        # 彈藥 圖標，跟他下方的 方形水域 的 Y座標偏移
        self.bulletOffset = 75
        # SKIP 右上角的 REGION 比率
        self.skip_appear_ratio_RightTop =  [ 0.879204, 0, 0.120796, 0.114634 ]
        # 主畫面左上角橘底白色N字 有後勤回來時候出現
        self.logistics_icon_ratio =  [ 0.034317, 0.185366, 0.035003, 0.040244 ]
        #主畫面右半邊的一個區塊
        self.main_window = [ 0.452986, 0.362195, 0.073439, 0.430488 ]
        # 主畫面，左邊 箭頭案開，紫色 軍事委託
        self.delegation_region_of_mainWindow_ratio =  [ 0.00892244, 0.30122, 0.398078, 0.168293 ]
        # 給智障後勤用的 螢幕比率參數 ...      	
        self.for_retardaDellegation_ratio = [ 0.149623, 0.104878, 0.777625, 0.853659 ]
        # 這個參數用來生成 ，實際海戰的 子區域
        self.true_of_battle_region_ratio =  [ 0.0981469, 0.0853659, 0.901853, 0.92561 ]
        # msg region，自律戰鬥時候 to be continue.... 的 訊息Region
        self.to_to_continue_msg_region_ratio =  [ 0.272478, 0.295122, 0.455045, 0.373171 ]
        # 6-4 比率，基本上  框框很小
        #self.stege_6_4_view_ratio =  [ 0.319835, 0.528049, 0.00686342, 0.0121951 ]
        # 在海戰畫面時，走到問號點 會觸發的 獲得道具 之圖標Region
        self.getToDaZe_region_ratio =  [ 0.388469, 0.217073, 0.229925, 0.206098 ]        
        #聰明後勤用來尋找可執行的委託的螢幕比率(下面)
        self.smartDellegation_below_ratio = [ 0.297872, 0.945122, 0.161977, 0.003659 ]