# -*- coding: UTF-8 -*-
from sikuli import *

###      <截圖原則>      ###
#
#     這邊的圖片 不要設定任何 相似度。這邊只有  
#     變數名A = "abc.png"
#       或
#     變數名B = ["efg.png"、"ijk.png"]
#
#     不要更動 等號 "左邊"的變數名稱
#
###      </截圖原則>      ###

# 主畫面的正方形 黃色 "出擊" 圖示
weigh_anchor = "weigh_anchor.png"
# 點選 敵方船艦 出現的 長方形 黃色 出擊
weigh_anchor_long = "weigh_anchor_long.png"
# 戰鬥畫面的 BOSS 圖案
boss_icon = "boss_icon.png"
# 小型規模，一個 黃色 三角形  海戰地圖。
mini_scale = "mini_scale.png"
# 中型規模，一個 黃色 三角形  海戰地圖。
middle_scale = "middle_scale.png"
# 大型規模，一個 黃色 三角形  海戰地圖。
big_scale = "big_scale.png"
# 去點一個敵方單位，但是船會過不去的敵方單位，出現的訊息
cannot_arrive_anemy = "cannot_arrive_anemy.png"
# 出現緊急委託 字樣
appear_Commission_msg = "appear_Commission_msg.png"
# 出現緊急委託 字樣 下方的確定、是否鎖定的確定、
# 撤退的確定、經驗減半的確定..
appear_Commission_msg_chk = "appear_Commission_msg_chk.png"
# 經驗減半，出擊時點黑的訊息
half_exp_msg = "half_exp_msg.png"
# 戰鬥勝利時 右下角 出現的  黃色確定
yellow_chk = "yellow_chk.png"
# 點擊以繼續，打贏時會在左下角出現的字樣
touch_to_continue = "touch_to_continue.png"
# 當你獲得一些獎勵時 出現的 點擊繼續
get_items_touch_to_continue = "get_items_touch_to_continue.png" 

# 當你獲得一個新的船艦時候 左下 會有 分享
share_when_get_new_ship = "share_when_get_new_ship.png"
# 使否鎖定該角色
chk_to_lock_ship_msg = "chk_to_lock_ship_msg.png"
# 選關卡時的 立即前往
immediate_start = "immediate_start.png"
# 2-4 約克鎮
stage_2_4 = "stage_2_4.png"
# 1-4 來自東方的艦隊
stage_1_4 = "stage_1_4.png"
# 水面艦隊，選關卡時 點下去時會看到
water_ship = "water_ship.png"
# 撤退
withdraw = "withdraw.png"
# 規避
avoid =  "avoid.png"
#彈藥的補給區
supplyBullet = "supplyBullet.png"
# 戰鬥中 右上角的 SKIP
skip_in_battle = "skip_in_battle.png"
# 進去委託介面，左上角的委託
dellgation_LT = "dellgation_LT.png"
# 在委託時會看到的加號
plus_icon_InLogisticsInterface = "plus_icon_InLogisticsInterface.png"
# 遠征條件無法滿足
cannot_enough_Logistics = "cannot_enough_Logistics.png"
# 前往，主畫面左上的箭頭案開，用於軍事委託、戰術學院
go_forward_icon = "go_forward_icon.png"
# 委託的取消，在委託介面裡面
dellgation_cancel = "dellgation_cancel.png" 
# 委託的推薦，在委託介面裡面
dellgation_susume = "dellgation_susume.png"
# 委託的開始，在委託介面裡面
dellgation_start = "dellgation_start.png"
# 船 追蝴蝶
retarded_ship_and_butterfly = "retarded_ship_and_butterfly.png"
# 我知道了
i_know_it = "i_know_it.png"
# 6-4 所羅門的惡夢
stage_6_4 = "stage_6_4.png" 
# 獲得道具
get_DAZE = "get_DAZE.png"
# 命運的五分鐘，撈SB2C用
stage_3_2 = "stage_3_2.png"
#====== 憲憲 =======

#剩餘彈藥圖示，會在海域內腳色船的頭上
remaining_Ammunition = "remaining_Ammunition.png"

#3-4 最後的反擊
stage_3_4= "Final_counterattack.png"

#主畫面右下角的"大艦隊"按鈕
big_fleet_button = "big_fleet_button.png"

#主畫面左上角出現後勤歸來的圖示
logistics_icon = "logistics_icon.png"

#後勤完成的按鈕
logistics_completion_button = "logistics_completion_button.png"

#後勤委託成功的畫面
Successful_commission = "Successful_commission.png"

#打開後勤選單的按鈕
open_logistics = "open_logistics.png"
# 紅染 普通C3 聖域的護衛
stage_C3 = "stage_C3.png"

# 紅染 普通D3 聖域紅染的守護者
stage_D3 = "stage_D3.png"

#合作社
merchant = "merchant.png"
#6-4 索羅門的噩夢
stage_6_4 =  "stage_6_4.png"
#3-2命運的五分鐘
stage_3_2 = "stage_3_2.png"
#5-1物資攔截
stage_5_1 = "stage_5_1.png"

# 各種後勤 ================================

# 日常資源開發 4 
# 獎勵 = "nichijyou_kaihatsu_4_reward.png"
nichijyou_kaihatsu_4 = "nichijyou_kaihatsu_4.png"
#未執行的後勤
unexecuted_Logistics = "unexecuted_Logistics.png"








