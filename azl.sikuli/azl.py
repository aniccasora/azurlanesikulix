# -*- coding: utf-8 -*-
#from sikuli import *
from time import sleep
import random
import subprocess
myScriptPath = ".\\"
addImportPath(myScriptPath)
import images,step,util
#  用 from XXX import XX 的話 呼叫時，可以 不用打 module name 
from someClasses import *
from util import ptlog,try_hl

# ★★★★★ <個人化設定 > ★★★★★★

# ★★★★★ </個人化設定> ★★★★★★


# ======== <全域變數> ============
GLOBAL_counter = 0

# ======== </全域變數> ============
def test(R):
    # 設定成 True 就會 直接執行 此區 code，用於測試自己的function 時。
    if True:
        util.ptlog("TEST=Start")
        #======================== test code
        ch = ConstantHolder()
        #=====================
        #主畫面自動收穫後勤

        
        # 永久自動後勤
        #step.retardaDellegation(R)
        
         #step.goInDellegation(R)
        
        #=====================
        #永久開始
        iii = 0
        while iii < 25:
            # NeedSupply = True ，代表會自己去補油
            # crazyMod = True 沒有彈藥照打
            #loop_X_X_stage(R, wantMap,mapChinese, magicRatio = None,Magic_X_Y = [0,0])
            #step.loop_X_X_stage(R, images.stage_2_4,"2-4 約克鎮",magicRatio=ch.stege_2_4_view_ratio,Magic_X_Y = [100,-50])
            # All_IN = True : 不管，彈藥，不管臉黑直接打歪
            # crazyMod = True 沒有彈藥照打
            step.loop_X_X_stage(R, images.stage_3_4,"3-4 最後的反擊",magicRatio=ch.stege_3_4_view_ratio,Magic_X_Y = [0,0], \
                    crazyMod = True,\
                    All_IN = True)  
            #step.loop_X_X_stage(R, images.stage_6_4,"6-4 所羅門的惡夢，記得要開掛",magicRatio=ch.stege_6_4_view_ratio ,Magic_X_Y = [138,111], \
                    #crazyMod = True,\
                    #All_IN = True)
            #step.loop_X_X_stage(R, images.stage_3_2,"3-2  命運的五分鐘，撈SB2C 飛機",magicRatio=ch.stege_6_4_view_ratio ,Magic_X_Y = [100,50], \
                    #crazyMod = True,\
                    #All_IN = True)
            #step.loop_X_X_stage(R, images.stage_3_4,"3-4 最後的反擊",magicRatio=ch.stege_3_4_view_ratio,Magic_X_Y = [0,0], NeedSupply = False)            
            #step.loop_X_X_stage(R, images.stage_1_4,"1-4 來自東方的艦隊",magicRatio = ch.stege_1_4_view_ratio)            
            #pass
            iii += 1
            ptlog("[round_complete]★★★★★★★ 第"+  str(iii)+"次完畢★★★★★★★★")
        # 永久結束    
        return 0
        util.ptlog("TEST=End")
        util.ptlog("wait 100 s ...")
        wait(100)
    pass

########## <觀察者事件>##########
#Obsever function

def appear_Commission(event):   
    ptlog("[Ober] : 偵測到 出現緊急委託!!")
    util.randClick(R,images.appear_Commission_msg_chk,1)
    ptlog("[Ober]:  已經嘗試按下確定...")
    event.repeat()
    
def appear_Skip(event):   
    ptlog("[Ober] : 偵測到 SKIP!!")
    util.randClick(R,images.skip_in_battle,1)
    ptlog("[Ober]:  已經嘗試按下SKIP...")
    util.randClick(R,images.appear_Commission_msg_chk,1)
    ptlog("[Ober]:  已經嘗試按下確定...")
    event.repeat()

def handle_retarded_ship_and_butterfly(event):
    ptlog("[Ober] :  智障船追花花!!")
    if R.exists( Pattern(images.i_know_it).similar(0.70),3):
        wait(1)
        getLastMatch().offset(149,-70).click()
        wait(1)
        
    util.randClick(R,images.i_know_it,3)
    
    ptlog("[Ober][warn] :  handle_retarded_ship_and_butterfly observer Stop!!")
    event.stopObserver()
    # 每次腳本 有按過就不用再檢查了
    #event.repeat()
    return 
def handle_get_DAZE(event)  :
    ptlog("[Ober] :  檢到咚咚!!")
    if R.exists(images.get_items_touch_to_continue,1):
        util.randClick(R,images.get_items_touch_to_continue,3)
    ptlog("[Ober] :  按完  點擊繼續了ㄅ!?") 
    wait(2)
    event.repeat()
########## </觀察者事件>##########

############# 主程式 ############# 
if __name__ == '__main__':

    # 參數物件 這個用來取一些參數用的。
    
    #呼叫util 內的 init() ，做一些環境參數的修改。
    util.init()
    
    # 設定一些 Region 。R  = 碧藍遊戲視窗
    # R = step.getMainRegion()
    
    # 先手動拉 之後正式跑就可以用上面的step.getMainRegion
    # PS: selectRegion() 拉出來的 不是Region??
    R = Region(selectRegion("Please select AzurLane Window"))
    
    
    # 決定是否開啟 baretail，由util 上方的 AUTO_OPEN_LOG_TRACER 決定!!
    util.init_baretail()
    ch = ConstantHolder()
    ##  <get_Observer_Region>
    # 取得 出現緊急委託 的region
    R_appear_Commission = step.getRegion_appear_Commission_msg(R) 
    # 取得 出現 SKIP 的 REGION
    R_skip_appear  = util.getRelativeSubRegion(R,ch.skip_appear_ratio_RightTop)
    # 取得 to be continue 的 msg region(EX: 你開自律時 會出現的那個 87船 追蝴蝶的 訊息 Region)
    R_to_be_continue = util.getRelativeSubRegion(R,ch.to_to_continue_msg_region_ratio)
    # 取得  在海戰畫面時，走到問號點 會觸發的 獲得道具 之圖標Region
    R_getToDaZe = util.getRelativeSubRegion(R,ch.getToDaZe_region_ratio)
    ##  </get_Observer_Region>
    
    ##  < 註冊 觀察事件>
    # 註冊 觀察事件。然後就開始觀察了。
    R_appear_Commission.onAppear(Pattern(images.appear_Commission_msg).similar(0.7), appear_Commission)
    R_appear_Commission.observeInBackground(FOREVER)

    R_skip_appear.onAppear(Pattern(images.skip_in_battle).similar(0.7), appear_Skip)
    R_skip_appear.observeInBackground(FOREVER)

    R_to_be_continue.onAppear(Pattern(images.retarded_ship_and_butterfly).similar(0.7), handle_retarded_ship_and_butterfly)
    R_to_be_continue.observeInBackground(FOREVER)

    R_getToDaZe.onAppear(Pattern(images.get_DAZE).similar(0.7), handle_get_DAZE)
    R_getToDaZe.observeInBackground(FOREVER)
    ##  </ 註冊 觀察事件>

    # ----------------------


    # 測試區塊，上方的if 條件打開就可以跑自己的測試code
    test(R)    
    # ----------------------
    while True :
        #計時開始
        tStart = time.time()
        # ----------------------
        # do something...
        # 
        # ----------------------
        #計時結束
        tEnd = time.time()
        #global GLOBAL_counter
        
        # 開啟檔案，紀錄執行次數用的
        fp=open('./time_log.txt','a')
        msg = time.strftime("%a %H:%M:%S", time.localtime()) + ', Execute time: ' + str(GLOBAL_counter) + '\n' 
        costTime  = "It cost " + str(tEnd - tStart) + 'sec' + '\n====================\n'
        fp.write(msg)
        fp.write(costTime)
        fp.close()
