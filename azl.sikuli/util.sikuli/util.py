# -*- coding: UTF-8 -*-
from sikuli import *
import random
import datetime
import time
import os,subprocess

myScriptPath = ".\\"
addImportPath(myScriptPath)
import images

# ★★★★★ 個人化設定 ★★★★★★
# log 的檔案名稱，此檔案會印出，所有執行的歷程。
# 改為 init() 執行時 自動 壓時戳 右邊的棄用---> ./log.txt
# 這邊 只是留下 變數宣告
log_FILENAME="XX" 
# 是否用 固定 log.txt, 如果希望 用動態命名的 log ， 請保持  (...= "")
USING_CONSTANT_LOG = "azu_Log.txt"
# 滑鼠壓下 放開延遲
MOUSE_CLICK_DELAY = 0.3
# 自動開啟 log監視器(baretail)，腳本資料夾(放有 sikulix.jar 檔案的)下需要有 baretail.exe
# AUTO_OPEN_LOG_TRACER = (True/False)
AUTO_OPEN_LOG_TRACER = True
# ----------------------------------


# 一些sikulix 的預設參數，在這邊做修改，跟一些需要初始化的行為
def init():
    # resize，決定 sikuli 在辨識畫面之前是否要先壓縮畫面，這樣可以提高效能，但降低準確率，預設開啟。
    Settings.AlwaysResize = True 
    #預設 find() 系列 function 最低準確度，IDE 的 "圖片比對預覽"不會調整，還是維持在 0.7喔。
    Settings.MinSimilarity = 0.7
    # mouse位置、點擊 log。在IDE下方會印出 綠色的log。
    Settings.ActionLogs= False
    # 未動
    Settings.InfoLogs = True
    # 未動
    Settings.DebugLogs = True
    # 滑鼠移動速度，x(秒) 內移動到指定位置。
    Settings.MoveMouseDelay = 0.25
    # 建立 log 檔名
    ts = time.time()
    st = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d_%H-%M-%S')
    global  log_FILENAME
    global  USING_CONSTANT_LOG
    if USING_CONSTANT_LOG == "":
        print "using dynamic file name!"
        log_FILENAME="./"+st+"_log.txt"
    else:
        print "USING_CONSTANT_LOG　setting!"
        log_FILENAME = USING_CONSTANT_LOG
    print ("使用:  " + str(log_FILENAME) + " 當作 log\n==============================")
    log_debug ("log_FILENAME : " + str(log_FILENAME))
    #  建立 log 檔案
    fp=open(log_FILENAME,'w')
    fp.close()
    pass
##########################################################
def init_baretail():
    global log_FILENAME
    global USING_CONSTANT_LOG
    if    AUTO_OPEN_LOG_TRACER and (USING_CONSTANT_LOG == ""):
        print ("Open baretail "+str(log_FILENAME))
        subprocess.Popen("baretail "+str(log_FILENAME))
    else:
        if AUTO_OPEN_LOG_TRACER == False:
            log_debug ("[info] util: AUTO_OPEN_LOG_TRACER is False !!")
        if (USING_CONSTANT_LOG is not ""):
            log_debug ("[info] util: USING_CONSTANT_LOG is not empty !!") 
        log_debug ("[info] util: CANNOT open baretail automatically.")
########################################################## 
# @exec  : 寫 log訊息 檔案，檔名上方的全域變數可以設定
# @param : msg (想要輸入的訊息)。
# @return: 無
def log_debug(msg):
    global  log_FILENAME
    fp=open(log_FILENAME,'a')
    # 寫入時戳
    fp.write(time.strftime("%H:%M:%S", time.localtime())+' : '+str(msg)+'\n')
    fp.close()

## overload
def ptlog(msg):
    global  log_FILENAME
    fp=open(log_FILENAME,'a')
    # 寫入時戳
    fp.write(time.strftime("%H:%M:%S", time.localtime())+' : '+str(msg)+'\n')
    fp.close()
##########################################################    
# img 參數 只接受字串(str)、list['str1','str2', ...]、Pattern。
# 只有將 img 轉為 Pattern 才可以指定相似度,img_P = Pattern(img).similar(0.8)。
# 然後再傳入 P， randClick(img_P) # 這樣就會以 80%相似度，找尋img_P(img)，並點選。
#
# @exec : 隨機點 img 區域內一下。只偵測在 region 內的 img。失敗時返回 False。
# @param : region (Region)。 限定 randClick 執行區域。
# @param : img (字串)(List)(Pattern,可給定相似度)
# @param : waitTime (int)。偵測 img 是否在 region， 之等待秒數。
# @return : 無
def randClick(region,img, waitTime):
    lpic = [] # empty list
    if isinstance(img,basestring):
        log_debug('[RC]: is_single, img 為單張圖片。')
        lpic[:0]=[img]
        
    elif isinstance(img,list):
        log_debug ('[RC]: is_list, img 為多張圖片!')
        lpic[:0] = img
        
    elif isinstance(img,Pattern):    
        log_debug( '[RC]: is sikuli.Pattern, img 為 Pattern 物件!')
        pictureName = getPatternLastFileName(img)
        log_debug("[RC]: Pattern filename: >"+pictureName+"<")
        if region.exists(pictureName, waitTime):
            log_debug("[RC]: Pattern(\'"+ pictureName +"\') is exists!")
        else:
            log_debug("[WARN][RC]: Pattern(\'"+ pictureName +"\') NOT exists!. RETURN False") 
            return False
        # 取得 符合該 Pattern 相似度之 Region
        new_R = getRegionByPattern(region,img)
        if new_R is not None:
            log_debug("[RC]: try to Click this Region,嘗試點擊此 Region(從 img 轉換而來)!")
            randClickRegion(new_R)
            log_debug("[RC]: 嘗試點擊此 Region 成功!")
            return True
        else:
            log_debug( '[WARN][RC]: Unable to find Region!,此相似度 Pattern 在 region內 找不到!')
            return False
    else:
        log_debug( '[Passing a Wrong Parameter][RC]: 你給到錯誤參數了!，估計是 第二個參數(img) 型態錯誤。')
        # 直接返回 不繼續執行
        return False
    # img 統一轉成 lpic(list)，做處理不管一個還是多個。
    for pic in lpic:
        if region.exists(pic, waitTime):
            log_debug("[RC]: \'"+str(pic)+"\' exists!")
            setThrowException(False)
            m = find(pic)
            if m is not None:
                pic_region = Region(m.getX(),m.getY(),m.getW(),m.getH())
                pic_region.highlight(1,"PINK")
                region.click(Location( random.randint(m.getX(),m.getX()+m.getW()),\
                                        random.randint(m.getY(),m.getY()+m.getH())))
                # 點到就可以回了，剩下的不用點了。
                log_debug("[RC]: out! (Img/List)")
                return True
            else:
                log_debug("[RC]: \'"+str(pic)+"\', CLICK_FAILED!")
        else:
            log_debug("[RC]: \'"+str(pic)+"\', region.exists() FAILED!")
##########################################################    
# 在給定的 region 隨機點一下  
# "#E4EB80" = "green1.png"
# @exec : 在給定的 target_region 內，點一下。因是 Region物件，不須檢查畫面。
# @param : target_region(Region)。你想要點的 Region 區域。
# @return : 無。
def randClickRegion(target_region):
    #target_region.highlight("#E4EB80")
    randPoint = genRandPositionByRegion(target_region)
    hover(randPoint)
    wait(MOUSE_CLICK_DELAY)
    # 謎之問題: 呼叫一次 click，是不會動的，叫兩次才會按一次...
    target_region.click(Mouse.at())
    target_region.click(Mouse.at())
    wait(MOUSE_CLICK_DELAY)
    log_debug("[RCR]: Click Region!--complete") 
    #target_region.highlight()
##########################################
# @param : 圖片,圖片list
# @region :  那張圖片的 region
# 用黑色框 #000000
# 支援 img_list 
# @exec :
# @param :
# @param :
# @return :
def getRegionByImages(R,img):
    log_debug("[util]:getRegionByImages()-- in!!")
    PList = [] # empty list
    ##############################    
    if isinstance(img,basestring):
        #log_debug("[util]:img convert to Pattern list...")
        #log_debug("show -> " + img + " !")
        PList[:0] = [Pattern(img)]
        #log_debug("show list-> " + str(PList) + " !")   
    elif isinstance(img,list):
        #log_debug("[util]:img list convert to Pattern list...")        
        for i in img:
            PList[:0] = [Pattern(i)]
    else:
        log_debug("[ERROR][util]:uncatch img type...")
        return
    for i in PList :
        log_debug("filename:  -> " + getPatternLastFileName(i) + " !")
    log_debug("[util]: 開始檢查是否存在?")

    for P in PList:
        if R.exists(P,3):
            log_debug("[util][found]:  找到了 !!") 
            R.getLastMatch().highlight(1)
            match = R.getLastMatch()
            res_region = Region(match.getX(),match.getY(),match.getW(),match.getH())
            log_debug("[util]:getRegionByImages()--show region(#000000)!!")
            res_region.highlight(1,"#000000")
            log_debug("[util]:getRegionByImages()--complete!!") 
            return res_region
        log_debug("[util]:find next of list!")
    log_debug("[ERROR][util]:not exists img...")
    return 
##########################################
# @exec : 拿region,在 R 內找到對應 Pattern的區域，相似度是找到的關鍵。
# @param : R (Region)，尋找範圍
# @param : pattern，想要找的的 Pattern物件
# @return :
def getRegionByPattern(R,pattern):
    funcName = "getRegionByPattern()"
    log_debug("[util]:"+funcName+"-- in !!")
    if isinstance(pattern,Pattern):
        log_debug("[util]:Is Pattern!!, 傳入參數正確!")
        P = pattern
    else:
        log_debug("[Passing a Wrong Parameter][util]: pattern參數傳遞錯誤!, RETURN None!")
        return None
    if R.exists(pattern):
        log_debug("[util]:this pattern is EXISTS!!, 可在此Region 找到對應 Pattern!")
    else:
        log_debug("[WARN][util]:this pattern ("+getPatternLastFileName(pattern)+") is NOT exists!!, 找不到符合 Pattern相似度的 Region!, RETURN None!")
        return None
    match = R.getLastMatch()
    res_region = Region(match.getX(),match.getY(),match.getW(),match.getH())
    log_debug("[util]:"+funcName+"--show region, 顯示找到 Region...[#000000]")
    res_region.highlight(1,"#000000")
    return res_region
##########################################
# 在給定的Region 內生成隨機點
# @exec :
# @param :
# @return :
def genRandPositionByRegion(region):
    if not isinstance(Region(region),Region):
        log_debug( "[ERROR][util]genRandPositionByRegion()--input region CAN'T NOT convert to Region!!!")
        return
    return Location ( random.randint(region.x,region.x+region.w),\
                                     random.randint(region.y,region.y+region.h))
##########################################
# @exec : 獲得 img, Pattern 的圖片原名稱。
# @param : PS
# @return : (String) EX:"XXXX.png"
def getPatternLastFileName(pattern):
    funcName = "getPatternLastFileName"
    log_debug("[util]:"+funcName+"--start!")
    PP = Pattern(pattern)
    if isinstance(pattern,Pattern):
        full_img_name = PP.getFilename()
        path_list = full_img_name.split('\\')
        pic = ""
        for i in path_list:
            pic = i
    elif isinstance(pattern, basestring):
        pic = pattern
    else:
        log_debug("[Passing a Wrong Parameter][util]: 不支援的傳入參數")
    log_debug("[util]:"+funcName+"--complete!")
    return str(pic)
##########################################
# @exec:  嘗試按下某一個 target 直到 wantAppearImg 出現,
#           等待 wantAppearImg 出現延遲為  waitTime
# @param : R(Region)。遊戲畫面
# @param : target(Region、Img(String))
# @param : wantAppearImg(String)，你要等待出現的圖片。
# @param : 等待秒數
# @return : 無
def click_it_until_appear(R,target,wantAppearImg,waitTime):
    funcName = "click_it_until_appear"
    log_debug("[util]:"+funcName+"--start!")
    if isinstance(target,basestring):
        log_debug("[util]:is String...")
        tmpWaitTime = 1
        P_Img = Pattern(wantAppearImg)
        while not R.exists(P_Img,tmpWaitTime):
            log_debug("[util]: 嘗試點選 -> " + target )
            tmpWaitTime = waitTime
            randClick(R,target,waitTime)
            log_debug("[retry][util]: 檢查 -> " + getPatternLastFileName(P_Img) + " 是否出現..." ) 
            wait(1)
        log_debug("[util]:"+funcName+"--complete!")    
        return 
    elif  isinstance(target,Region):
        log_debug("[util]:is Region...")
        while True:
            log_debug("[util]:嘗試點選 targetRegion...")
            randClickRegion(target)
            wait(waitTime)
            log_debug("[util]:檢查是否出現目標圖片。等待延遲為="+str(waitTime) +" 秒...")
            if R.exists(wantAppearImg,waitTime):
                log_debug("[util]:"+funcName+"--complete!")
                return
            else:
                log_debug("[try_again][util]: 再次尋找目標圖片...")
                continue
    else:
        log_debug("[Passing a Wrong Parameter][util]: 錯誤的 target 參數...")
##########################################
# 支援 img(String)、Pattern
# @exec : 按下指定的 PS 直到 其消失為止。
# @param : R(Region)。遊戲畫面
# @param : PS(Region、img(String))。你要按下的圖標，不要傳入 Region。
# @param : actionCycle(int)。執行週期。
# @param : delay(int)。等待消失秒數，如果在 delay秒沒有消失 waitVanish()回傳 false。
# @return : 無
def click_it_until_vanish(R,PS,actionCycle,delay = 3):
    funcName = "click_it_until_vanish()"
    log_debug("[util]:"+funcName+"--start!")
    PS_name =  getPatternLastFileName(PS) 
    log_debug("[util]: delay = "+str(delay)+" sec,  clickTarget--> "+ PS_name )
    cnt=0
    while not R.waitVanish(PS,delay):
        cnt+=1
        delay=actionCycle
        log_debug("[util]: 目標未消失, 嘗試點擊--> \'"+PS_name+"\'")
        randClick(R,PS,actionCycle)
        log_debug("[util][WARN]: "+ str(delay)  +" 秒內檢查--> \'"+PS_name+"\' 是否消失...")
        wait(delay)
    if cnt is 0:
        log_debug("[WARN][util]: 此圖根本不存在於畫面上!! ("+PS_name+")")
    log_debug("[util]:"+funcName+"--complete!")
##########################################
# 有個 input List:  第[0]個:0.1253、第[1]個:0.2365、第[2]個:0.2265
# 呼叫 getMaxNumberIdxByList(input) ----> output : 1
# @exec : 從 list 之中取得最大的 數字的 idx(索引值) 
# @param : list
# @return : maxIdx(int), 介於( 0~串列長度-1 )之間 的數字 
def getMaxNumberIdxByList(numberList):
    res = -1.0
    maxIdx = -1
    for idx,n in enumerate(numberList):
        if  float(n) > res:
            res = float(n)
            maxIdx = idx 
    return maxIdx
##########################################    
# @exec :  "每秒" 檢查一次，如果在 continueAppearTime 內 ，都一直存在給定 PS 的話，就會放行。
# @param : R(Region)。主畫面。
# @param : PS(PS)，在畫面 持續存在的 圖片(PS)。
# @param : continueAppearTime(int)。持續檢查時間。
# @return : 無
def wait_PS_KeepExistsInTime(R,PS,continueAppearTime):# util 模板
    funcName = "wait_PS_KeepExistsInTime()"
    log_debug("[util]:"+funcName+"--start!")
    if isinstance(PS,basestring) or isinstance(PS,Pattern):
        pass
    else:
        log_debug("[Passing a Wrong Parameter][util]:"+funcName+"--RETURN!!")
        return
    totalWaitTime = 0
    log_debug("[util]:"+funcName+"--共需等待 "+ str(continueAppearTime) + " 秒...")
    log_debug("[util]:   ~~wait Target~~  -- > \'" + getPatternLastFileName(PS) + "\'")
    #token 用來控制訊息(log)印出用
    token = -1
    while totalWaitTime != continueAppearTime :
        if R.exists(PS,1) :
            totalWaitTime +=1
            wait(1)
            log_debug("[util]:"+funcName+"--已經等待 "+ str(totalWaitTime) + " 秒...")
            token = 1
        else:
            totalWaitTime = 0
            if token is 1:
                log_debug("[util]:"+funcName+"--中斷!! , 重新計算 "+ str(continueAppearTime) + " 秒...")
                token = 0
    log_debug("[util]:"+funcName+"--complete!")
##########################################

# @exec : 獲得相對於 mainRegion，子區域
# @param : mainRegion(Region) 
# @param : [x比率,y比率,w比率,h比率](List)，就4個數字(0~1)之間。
# @return  : subRegion(Region)
def getRelativeSubRegion(mainRegion,ratiosList):
    funcName = "getRelativeSubRegion()"
    log_debug("[util]:"+funcName+"--start!")
    log_debug("[util]: ratiosList [x, y, w, h] = " +
            "[ "+str(ratiosList[0])+", "+\
                    str(ratiosList[1])+", "+\
                    str(ratiosList[2])+", "+\
                    str(ratiosList[3])+" ]")
    subRegion =Region(\
        int(mainRegion.getW()*ratiosList[0]+mainRegion.getX()),\　
        int(mainRegion.getH()*ratiosList[1]+mainRegion.getY()),\
        int(mainRegion.getW()*ratiosList[2]),\
        int(mainRegion.getH()*ratiosList[3])) 
    log_debug("[util]:顯示 得到的 subRegion 2 秒... 綠色#5AC34A ")
    log_debug("[util]: newSubRegion [x, y, w, h] = "+\
        "[ "+str(subRegion.getX())+", "+\
                str(subRegion.getY())+", "+\
                str(subRegion.getW())+", "+\
                str(subRegion.getH())+" ]")
    subRegion.highlight(0.5,"#5AC34A")
    log_debug("[util]:"+funcName+"--complete!")
    return subRegion
########################################## 
# 如想寫下新的 util 功能 複製以下 程式模板
# @exec :
# @param :
# @return :
def example():
    funcName = "example()"
    log_debug("[util]:"+funcName+"--start!")
    # 寫下你的 code...

    # 不要依賴 此檔案以外的參數，不得出現 step.XXXX or images.XXXX。
    # 因為這支檔案以後抽開來用的話，step、images，不一定存在。
    log_debug("[util]:"+funcName+"--complete!")
########################################## 
# 傳入一個 物件 嘗試 highlight 他，如果不行 他會印在 log 上面
def  try_hl(inputO, hltime = 1, color = "#FFFFFF",toggle = "F"):

    if   (isinstance(inputO, Region) ):
        if (toggle == "F"):
            inputO.highlight(hltime,color)
        else:
            inputO.highlight(color)
    elif( isinstance(inputO, Match) ):
        if (toggle== "F"):
            inputO.highlight(hltime,color)
        else:
            inputO.highlight(color)
    else:
        ptlog("[warn] [util] : try_highlight()  failed...")
        # 解釋為何 無法 HL
        if (  inputO is  None):
           ptlog("[warn] [util] :  by 你他媽 這根本是 None 物件....")
        elif (  isinstance(inputO, Pattern)):
           ptlog("[warn] [util] :  by 你他媽 這根本是 Pattern 物件....") 
        else:
           ptlog("[warn] [util] :  by 未知的原因....")  
##########################################
def list_process():
    c = wmi.WMI ()
    for process in c.Win32_Process ():
        print process.ProcessId, process.Name
