#include<iostream>
#include <string>
#include <vector>
#include <iomanip>
using namespace std;

# define WindowsX = 1920
# define WindowsY = 1080

size_t split(const string &txt, vector<string> &strs, char ch)
{
    size_t pos = txt.find( ch );
    size_t initialPos = 0;
    strs.clear();

    // Decompose statement
    while( pos != string::npos ) {
        strs.push_back( txt.substr( initialPos, pos - initialPos ) );
        initialPos = pos + 1;

        pos = txt.find( ch, initialPos );
    }

    // Add the last one
    strs.push_back( txt.substr( initialPos, min( pos, txt.size() ) - initialPos + 1 ) );

    return strs.size();
}

string trim(string s){ 
    if (s.empty()) {
        return s;
    }
    s.erase(0,s.find_first_not_of(" "));
    s.erase(s.find_last_not_of(" ") + 1);
    return s;
}


int main(int argc,char * argv[]) {
	
    if ( !argv[1] || !argv[2] ) return -1;

    int gameW_X = atoi(argv[1]);
    int gameW_Y = atoi(argv[2]);
    
    cout << "Game Windows SIZE (Width, Height): " << gameW_X << ", " << gameW_Y << endl;
    cout << "---------------------------------\n";
    while (true){
        char input[256];
        cout << "靠左 寬度 靠上 高度:";
        cin.getline(input,256);
        
        vector<string> v;
        vector<double> result;
        
        string new_input = trim(input);
        split( new_input, v, ' ');

        if (v.size() != 4) {cout << "數量錯誤!!" << endl;continue;}
        
        result.push_back( atof(v.at(0).c_str()) / gameW_X);
        result.push_back( atof(v.at(2).c_str()) / gameW_Y);
        result.push_back( atof(v.at(1).c_str()) / gameW_X);
        result.push_back( atof(v.at(3).c_str()) / gameW_Y);

        cout << " [ ";
        for (auto i=0;i<result.size()-1;i++){
            cout << result.at(i) << ", ";
        }cout << setprecision(6)  <<result.at(result.size()-1) << " ]\n\n";
    }
    

    return 0;
}
